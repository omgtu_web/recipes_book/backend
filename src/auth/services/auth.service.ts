import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthResponse, AuthDto } from '../auth.dto';
import * as crypto from 'crypto';
import { UsersService } from 'src/users/services/users.service';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(dto: AuthDto): Promise<AuthResponse> {
    let user: User;
    try {
      user = await this.usersService.create(dto);
    } catch (error) {
      throw new BadRequestException('User already exist');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    return { jwt, user } as AuthResponse;
  }

  async signIn(dto: AuthDto): Promise<AuthResponse> {
    const password = crypto.createHmac('sha256', dto.password).digest('hex');
    const user = await this.usersService.getByUsernameAndPassword(dto.username, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    return { jwt, user } as AuthResponse;
  }
}
