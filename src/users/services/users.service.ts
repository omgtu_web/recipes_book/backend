import { Injectable } from '@nestjs/common';
import { CreateUserDto } from '../dtos/users.dto';
import { User } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  async getByUsernameAndPassword(username: string, password: string): Promise<User> {
    return this.usersRepository.findOne({ where: { username, password } });
  }

  async getOneOrFail(id: string): Promise<User> {
    return this.usersRepository.findOneOrFail({ where: { id } });
  }

  async create(user: CreateUserDto): Promise<User> {
    return this.usersRepository.save(this.usersRepository.create(user));
  }
}
