import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthDto, AuthResponse } from 'src/auth/auth.dto';
import { AuthService } from 'src/auth/services/auth.service';
import { IAM } from 'src/common/decorators/iam.decorator';
import { User } from '../entities/user.entity';

@ApiTags('User')
@Controller('users')
export class UsersController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Login' })
  @ApiBody({ type: AuthDto })
  @ApiResponse({ status: 200, type: AuthResponse })
  @Post('sign-up')
  async signUp(@Body() input: AuthDto): Promise<AuthResponse> {
    return this.authService.signUp(input);
  }

  @ApiOperation({ summary: 'Register' })
  @ApiBody({ type: AuthDto })
  @ApiResponse({ status: 200, type: AuthResponse })
  @Post('sign-in')
  async signIn(@Body() input: AuthDto): Promise<AuthResponse> {
    return this.authService.signIn(input);
  }

  @ApiOperation({ summary: 'Get current user' })
  @ApiResponse({ status: 200, type: User })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  async getMe(@IAM() user: User) {
    return user;
  }
}
