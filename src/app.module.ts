import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { DB_URL } from './config';
import { Recipe } from './recipes/entities/recipes.entity';
import { RecipesModule } from './recipes/recipes.module';
import { User } from './users/entities/user.entity';
import { UserModule } from './users/user.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    RecipesModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: DB_URL,
      logging: true,
      entities: [User, Recipe],
      synchronize: true,
    }),
  ],
})
export class AppModule {}
