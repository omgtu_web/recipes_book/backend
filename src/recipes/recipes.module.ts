import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { RecipesController } from "./controllers/recipes.controller";
import { Recipe } from "./entities/recipes.entity";
import { RecipesService } from "./services/recipes.services";

@Module({
  imports: [TypeOrmModule.forFeature([Recipe])],
  controllers: [RecipesController],
  providers: [RecipesService],
  exports: [RecipesService],
})
export class RecipesModule {}