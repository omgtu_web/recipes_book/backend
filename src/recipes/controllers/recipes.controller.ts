import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from "@nestjs/swagger";
import { IAM } from "src/common/decorators/iam.decorator";
import { User } from "src/users/entities/user.entity";
import { CreateRecipeDto } from "../dtos/create-recipe.dto";
import { UpdateRecipeDto } from "../dtos/update-recipe.dto";
import { Recipe } from "../entities/recipes.entity";
import { RecipesService } from "../services/recipes.services";

@ApiTags('Recipes')
@Controller('recipes')
export class RecipesController {
  constructor(private readonly recipeService: RecipesService) {}

  @ApiBody({ type: CreateRecipeDto })
  @ApiResponse({ status: 200, type: Recipe })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@IAM() iam: User, @Body() input: CreateRecipeDto): Promise<Recipe> {
    return this.recipeService.create(iam, input);
  }
  
  @ApiResponse({ status: 200, type: Recipe, isArray: true })
  @Get()
  async find(): Promise<Recipe[]> {
    return this.recipeService.find();
  }
  
  @ApiBody({ type: UpdateRecipeDto })
  @ApiResponse({ status: 200, type: Recipe })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(@IAM() user: User, @Body() input: UpdateRecipeDto, @Param('id') id: string): Promise<Recipe> {
    return this.recipeService.update(user, id, input);
  }
  
  @ApiResponse({ status: 200, type: Boolean })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async delete(@IAM() user: User, @Param('id') id: string): Promise<boolean> {
    return this.recipeService.delete(user, id);
  }
}