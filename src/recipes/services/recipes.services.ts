import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/users/entities/user.entity";
import { Repository } from "typeorm";
import { CreateRecipeDto } from "../dtos/create-recipe.dto";
import { UpdateRecipeDto } from "../dtos/update-recipe.dto";
import { Recipe } from "../entities/recipes.entity";

@Injectable()
export class RecipesService {
  constructor(
    @InjectRepository(Recipe)
    private readonly recipesRepository: Repository<Recipe>,
  ) {}

  async create(user: User, dto: CreateRecipeDto): Promise<Recipe> {
    return this.recipesRepository.save({ ...dto, authorId: user.id });
  }

  async find(): Promise<Recipe[]> {
    return this.recipesRepository.find({ relations: ['author'] });
  }

  async update(user: User, id: string, dto: UpdateRecipeDto): Promise<Recipe> {
    const recipe = await this.recipesRepository.findOne({ where: { id, authorId: user.id }, select: ['id'] });
    return this.recipesRepository.save({ ...dto, id: recipe.id });
  }

  async delete(user: User, id: string): Promise<boolean> {
    try {
      await this.recipesRepository.delete({ id, authorId: user.id });
      return true;
    } catch (error) {
      return false;
    }
  }
}