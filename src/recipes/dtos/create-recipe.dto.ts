import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsNotEmpty, IsString } from "class-validator";

export class CreateRecipeDto {
  @ApiProperty({ example: 'Бутерброд', description: 'Название блюда' })
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({ example: 'Порезать хлеб, колбасу и с майонезом', description: 'Рецепт' })
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  text: string;
}